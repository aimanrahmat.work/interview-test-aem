import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var $: any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit {

  public formRegister: FormGroup;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.formRegister = this.formBuilder.group({
      name: this.formBuilder.group({
        first: '',
        last: ''
      }),
      email: '',
      phone: this.formBuilder.group({
        mobile: ''
      }),
      skills: this.formBuilder.array([])
    });    
    
  }
}
